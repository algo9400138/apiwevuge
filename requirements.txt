fastapi==0.103.1
firebase_admin==6.2.0
hypertrack==3.0.4
mysql_connector_repackaged==0.3.1
Requests==2.31.0
SQLAlchemy==2.0.19
starlette==0.31.1
uvicorn==0.23.2
