#from functools import partial
#from http.server import HTTPServer, SimpleHTTPRequestHandler

#handler = partial(SimpleHTTPRequestHandler, directory='./public')
#server_object = HTTPServer(('', 3000), handler)
#server_object.serve_forever()
import uvicorn
from main import app

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=3000)